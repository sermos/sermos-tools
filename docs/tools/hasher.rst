.. _tools-hasher:

=======
 Hasher
=======

.. currentmodule: sermos_tools.catalog.hasher

.. automodule:: sermos_tools.catalog.hasher.hasher

.. autoclass:: sermos_tools.catalog.hasher.hasher.Hasher
