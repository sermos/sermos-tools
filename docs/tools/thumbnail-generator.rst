.. _tools-thumbnail-generator:

====================
 Thumbnail Generator
====================

.. currentmodule: sermos_tools.catalog.thumbnail_generator

.. automodule:: sermos_tools.catalog.thumbnail_generator.thumbnail_generator

.. autoclass:: sermos_tools.catalog.thumbnail_generator.thumbnail_generator.ThumbnailGenerator
