.. _tools:

=====
Tools
=====

Sermos comes full of a growing library of `tools` that take care of mundane
and common tasks in Machine Learning (ML) and Natural Language Processing (NLP)
workloads.

These tools are automatically injected into all of your API classes and worker
methods so you don't need to worry about reinventing the wheel.

.. toctree::
    :maxdepth: 1

    date-extractor
    document-classifier
    hasher
    language-detector
    ner-tool
    slack-notifier
    sparse-embedding-search
    text-extractor
    tfidf-vectorizer
    thumbnail-generator
