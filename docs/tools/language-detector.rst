.. _tools-language-detector:

==================
 Language Detector
==================

.. currentmodule: sermos_tools.catalog.language_detector

.. automodule:: sermos_tools.catalog.language_detector.language_detector

.. autoclass:: sermos_tools.catalog.language_detector.language_detector.LanguageDetector
