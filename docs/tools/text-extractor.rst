.. _tools-text-extractor:

===============
 Text Extractor
===============

.. currentmodule: sermos_tools.catalog.text_extractor

.. automodule:: sermos_tools.catalog.text_extractor.text_extractor

.. autoclass:: sermos_tools.catalog.text_extractor.text_extractor.TextExtractor
