.. _index:

.. image:: _static/sermos.png

============================================
 Machine Learning Tools for Enterprise Scale
============================================

:Release: |version|
:Date: |today|

----

Sermos Tools is a curated set of tooling that is purpose-built for making
it easier and more efficient to integrate Machine Learning (ML) techniques
including Natural Language Processing (NLP), Computer Vision (CV),
Decision Modeling, and more into real-world applications.

These tools are based on years of production implementations across industries
spanning healthcare, finance, motorsports, recycling, water, and energy. Our
goal is to provide an open source catalog of tools that enable organizations
to bridge the divide that exists between data science and actual use-cases
that make an impact.

========
Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Core

   tools/index

.. toctree::
   :maxdepth: 1
   :caption: Related Libraries

   Sermos <https://docs.sermos.ai>
   Rho ML <https://docs.rho.ai/rho-ml>

* :ref:`genindex`
* :ref:`modindex`
