""" Test classes/functions in sermos_tools.py
"""
import os
from importlib import import_module
from sermos_tools.sermos_tools import (SermosTool, is_capital_case,
                                       is_snake_case, capital_to_snake,
                                       snake_to_capital, list_available_tools,
                                       retrieve_tool_class, SermosTools)


class TestSermosTools:
    """ Test Sermos Tools
    """
    names = ('CapitalCase', 'camelCase', 'snake_case', 'snakecase',
             '1numericsnake_case')

    def test_sermos_tool(self):
        """ Base wrapper
        """
        st = SermosTool()
        assert st.name() == 'SermosTool'

        class MyTool(SermosTool):
            pass

        assert MyTool().name() == 'MyTool'

    def test_is_capital_case(self):
        """ # TODO - good case to implement hypothesis """
        assert is_capital_case(self.names[0]) is True
        assert is_capital_case(self.names[1]) is False
        assert is_capital_case(self.names[2]) is False
        assert is_capital_case(self.names[3]) is False
        assert is_capital_case(self.names[4]) is False

    def test_is_snake_case(self):
        """ # TODO - good case to implement hypothesis """
        assert is_snake_case(self.names[0]) is False
        assert is_snake_case(self.names[1]) is False
        assert is_snake_case(self.names[2]) is True
        assert is_snake_case(self.names[3]) is True
        assert is_snake_case(self.names[4]) is True

    def test_capital_to_snake(self):
        """ # TODO - good case to implement hypothesis """
        assert capital_to_snake('MyCapital') == 'my_capital'

    def test_snake_to_capital(self):
        """ # TODO - good case to implement hypothesis """
        assert snake_to_capital('snake_case') == 'SnakeCase'

    def test_list_available_tools(self):
        """ """
        tools = list_available_tools()
        assert type(tools[0]) == str

        tools = list_available_tools(as_dict=True)
        assert type(tools) == dict
        assert len(tools.keys()) > 0

    def test_retrieve_tool_class(self):
        """ """
        # snake_case
        tool = retrieve_tool_class('hasher')
        assert issubclass(tool, SermosTool)
        assert tool.name() == 'Hasher'

        # CapitalCase
        tool = retrieve_tool_class('Hasher')
        assert issubclass(tool, SermosTool)
        assert tool.name() == 'Hasher'

    def test_sermos_tools(self):
        """ Test SermosTools class """

        # Wrapper starts with no tools
        st = SermosTools()
        assert len(st.available_tools) == 0

        # Manually set a tool
        hasher = retrieve_tool_class('hasher')
        st.set_tool(hasher)
        assert len(st.available_tools) == 1
        assert st.available_tools[0] == 'Hasher'

        # Retrieve that tool (capital and snake cases)
        hasher2 = st.get('hasher')
        assert hasher2.name() == 'Hasher'
        hasher3 = st.get('Hasher')
        assert hasher3.name() == 'Hasher'

    def test_sermos_tools_load(self):
        """ Test SermosTools class """

        # Wrapper starts with no tools
        st = SermosTools()
        assert len(st.available_tools) == 0

        # Load a tool by requesting it
        hasher = st.get('hasher')
        assert hasher.name() == 'Hasher'
