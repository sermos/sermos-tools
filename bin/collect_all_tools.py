""" Collect all tool names and return as comma separated string
"""
from sermos_tools.sermos_tools import list_available_tools


def main():
    tools = list_available_tools()
    tools_str = ",".join(tools)
    print(tools_str)
    return tools_str


if __name__ == '__main__':
    main()
